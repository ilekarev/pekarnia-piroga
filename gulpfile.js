const
	gulp           = require('gulp'),
	gutil          = require('gulp-util' ),
	sass           = require('gulp-sass'),
	browserSync    = require('browser-sync'),
	cleanCSS       = require('gulp-clean-css'),
	autoprefixer   = require('gulp-autoprefixer'),
	bourbon        = require('node-bourbon'),
	ftp            = require('vinyl-ftp'),
	babel          = require('gulp-babel');

// Обновление страниц сайта на локальном сервере
gulp.task('browser-sync', function() {
	browserSync({
		proxy: "piroga.loc",
		open: false,
		notify: false
	});
});

// Компиляция stylesheet.css
gulp.task('sass', function() {
	return gulp.src('catalog/view/theme/piroga/stylesheet/stylesheet.sass')
		.pipe(sass({
			includePaths: bourbon.includePaths
		}).on('error', sass.logError))
		.pipe(autoprefixer(['last 15 versions']))
		.pipe(cleanCSS())
		.pipe(gulp.dest('catalog/view/theme/piroga/stylesheet/'))
		.pipe(browserSync.reload({stream: true}))
});

gulp.task('js', () =>
    gulp.src('catalog/view/javascript/newJS/*.js')
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulp.dest('catalog/view/javascript/'))
);

// Наблюдение за файлами
gulp.task('watch', ['sass', 'js', 'browser-sync'], function() {
	gulp.watch('catalog/view/theme/piroga/stylesheet/**/*.sass', ['sass']);
	gulp.watch('catalog/view/javascript/newJS/*.js', ['js']);
	gulp.watch('catalog/view/theme/piroga/template/**/*.twig', browserSync.reload);
	gulp.watch('catalog/view/theme/piroga/js/**/*.js', browserSync.reload);
	gulp.watch('catalog/view/theme/piroga/libs/**/*', browserSync.reload);
});

// Выгрузка изменений на хостинг
gulp.task('deploy', function() {
	var conn = ftp.create({
		host:      'hostname.com',
		user:      'username',
		password:  'userpassword',
		parallel:  10,
		log: gutil.log
	});
	var globs = [
	'catalog/view/theme/piroga/**'
	];
	return gulp.src(globs, {buffer: false})
	.pipe(conn.dest('/path/to/folder/on/server'));
});

gulp.task('default', ['watch']);
