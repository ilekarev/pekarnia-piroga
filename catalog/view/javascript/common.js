function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

function billReload(card_id, addOrRemove) {
    var elData = document.getElementById('js-input-cart-id' + card_id);

    var quantity = elData.value;
    var itemPrice = parseInt(elData.getAttribute('data-price'));
    var price = quantity * itemPrice;
    var total = parseInt(document.getElementById('js-bill-total-price').innerText) + itemPrice * addOrRemove;
    
    document.getElementById('js-bill-quantity-' + card_id).innerText = quantity + ' шт.';
    document.getElementById('js-bill-price-' + card_id).innerText = price + ' руб.';
    document.getElementById('js-bill-total-price').innerText = total + ' руб.';

    var checkoutButton = $('#js-checkout-button');

    if (checkoutButton != null) {
        if (total >= 600) {
            checkoutButton.attr('disabled', false);
            checkoutButton.attr('href', checkoutButton.attr('data-href'));
            checkoutButton.text('Оформить заказ')
        } else {
            checkoutButton.attr('disabled', true);
            checkoutButton.removeAttr('href');
            checkoutButton.text('Минимальный заказ 600 руб.')
        }
    }

}

document.addEventListener('DOMContentLoaded', function() {
    topBasket.init();
})

var topBasket = {
    element: null,
    elQuantity: null,
    elPrice: null,

    quantity: 0,
    price: 0,

    add: function(price) {
        this.addQuantity(1);
        this.addPrice(price);
    },

    remove: function(price) {
        if (this.quantity) {
            this.addQuantity(-1);
            this.addPrice(-price);
        }
    },

    addQuantity: function(quantity) {
        if (typeof quantity !== 'number') return false;

        this.quantity += quantity;

        this.elQuantity.innerHTML = this.quantity;
        this.elQuantity.setAttribute('data-total-quantity', this.quantity);
    },

    addPrice: function(price) {
        if (typeof price !== 'number') return false;

        this.price += price;

        this.elPrice.innerHTML = this.price + ' р';
        this.elPrice.setAttribute('data-price', this.price);
    },

    init: function() {
        this.element = document.getElementById('js-top-basket');
        this.elQuantity = this.element.querySelector('#js-cart-info-total');
        this.elPrice = this.element.querySelector('#js-cart-info-price')

        this.quantity = parseInt(this.elQuantity.getAttribute('data-total-quantity'));
        this.price = parseInt(this.elPrice.getAttribute('data-price'));
    },
}

function increment(el, option) {

    // Отправляем запрос "добавить в корзину"
    cart.add(el, option);

    element = $('#js-quantity' + el);

    var n = parseInt(element.val())

    if (n < 99) {
        n++;

        var price = parseInt(element.attr('data-price'));

        topBasket.add(price)

    }
}

function decrement(cartId, quantity) {

    var formData = new FormData();
    formData.append('quantity[' + cartId + ']', quantity);

    var xhr = new XMLHttpRequest();

    xhr.open("POST", "index.php?route=checkout/cart/edit");
    xhr.send(formData);

}

$(document).ready(function() {

    // popup-send-message
    $(document).ready(function() {
        $('.send-message-link').magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#email'

        });
    });

    $('#popup-send-message').submit(function() {
        var th = $(this);
        console.log('click');
        sendData(th);

        return false;
    });

    function sendData(th) {
        $.ajax({
            type: "POST",
            url: "catalog/view/mail.php",
            data: th.serialize()
        }).done(function() {
            console.log("Сообщение отправлено!");
            setTimeout(function() {
                th.trigger("reset");
                $.magnificPopup.close();
            }, 1000);
        });
    }


    // Highlight any found errors
    $('.text-danger').each(function() {
        var element = $(this).parent().parent();

        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });

    // Currency
    $('#form-currency .currency-select').on('click', function(e) {
        e.preventDefault();

        $('#form-currency input[name=\'code\']').val($(this).attr('name'));

        $('#form-currency').submit();
    });

    // Language
    $('#form-language .language-select').on('click', function(e) {
        e.preventDefault();

        $('#form-language input[name=\'code\']').val($(this).attr('name'));

        $('#form-language').submit();
    });

    /* Search */
    $('#search input[name=\'search\']').parent().find('button').on('click', function() {
        var url = $('base').attr('href') + 'index.php?route=product/search';

        var value = $('header #search input[name=\'search\']').val();

        if (value) {
            url += '&search=' + encodeURIComponent(value);
        }

        location = url;
    });

    $('#search input[name=\'search\']').on('keydown', function(e) {
        if (e.keyCode == 13) {
            $('header #search input[name=\'search\']').parent().find('button').trigger('click');
        }
    });

    // Menu
    $('#menu .dropdown-menu').each(function() {
        var menu = $('#menu').offset();
        var dropdown = $(this).parent().offset();

        var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

        if (i > 0) {
            $(this).css('margin-left', '-' + (i + 10) + 'px');
        }
    });

    // Product List
    $('#list-view').click(function() {
        $('#content .product-grid > .clearfix').remove();

        $('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');
        $('#grid-view').removeClass('active');
        $('#list-view').addClass('active');

        localStorage.setItem('display', 'list');
    });

    // Checkout
    $(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function(e) {
        if (e.keyCode == 13) {
            $('#collapse-checkout-option #button-login').trigger('click');
        }
    });

    $(window).scroll(function() {

        if (window.innerWidth < 768) {
            return;
        }

        $('#js-top-line').parent('header').css({
            height: ($('#js-top-line').height() + 20) + 'px',
        });

        if ($(window).scrollTop() > 1000) {

            $('#js-top-line').css({
                position: 'fixed',
                zIndex: '1',
                top: 0,
                left: 0,
                right: 0,
                boxShadow: '0 0 6px 0 rgba(0, 0, 0, .6)',
            });

        } else {

            $('#js-top-line').css({
                position: '',
                boxShadow: '',
            });
        }
    });
});

// Cart add remove functions
var cart = {
    'add': function(product_id, optionId, optionValueId) {

        var hasOptions = typeof optionId !== 'undefined' && typeof optionValueId !== 'undefined' ? true : false;

        var data = 'product_id=' + product_id + '&quantity=' + 1;

        if (hasOptions) {
            data += '&option[' + optionId + ']=' + optionValueId;
        }

        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function(json) {

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {

                    var result = json['cartProducts'];

                        result.forEach(function(element) {

                            if (hasOptions) {

                                element.options.forEach(function(option) {
    
                                    var resOptionId       = option['product_option_id'],
                                        resOptionValueId  = option['product_option_value_id'];

                                    if (optionId === resOptionId && resOptionValueId === optionValueId) {

                                        var elementDataCartList = document
                                            .getElementById('js-option-id-' + optionId)

                                        if (elementDataCartList === null) return false;

                                        elementDataCartList = elementDataCartList.querySelectorAll('.js-cart-input');

                                        for (var i = 0; i < elementDataCartList.length; i++) {
                                            
                                            var elementDataCart = elementDataCartList[i];

                                            if (elementDataCart.getAttribute('data-option-value-id') === optionValueId) {
                                                elementDataCart.setAttribute('data-cart-id', element.cart_id);

                                                break;
                                            }
                                        }   
                                    } 
    
                                });
    
                            } else {
                                // Если нет опций:

                                var elementDataCart = document.getElementById('js-quantity' + product_id);

                                if (elementDataCart === null) return false;

                                elementDataCart.setAttribute('data-cart-id', element.cart_id);
                            }
                        });

                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function(key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-basket"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-basket"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
};

var voucher = {
    'add': function() {

    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-basket"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
};

var wishlist = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);

                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
};

var compare = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert-dismissible').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function() {

    }
};

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function(data) {
            html  = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div>';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});

// Autocomplete */
(function($) {
    $.fn.autocomplete = function(option) {
        return this.each(function() {
            this.timer = null;
            this.items = new Array();

            $.extend(this, option);

            $(this).attr('autocomplete', 'off');

            // Focus
            $(this).on('focus', function() {
                this.request();
            });

            // Blur
            $(this).on('blur', function() {
                setTimeout(function(object) {
                    object.hide();
                }, 200, this);
            });

            // Keydown
            $(this).on('keydown', function(event) {
                switch(event.keyCode) {
                    case 27: // escape
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });

            // Click
            this.click = function(event) {
                event.preventDefault();

                value = $(event.target).parent().attr('data-value');

                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            };

            // Show
            this.show = function() {
                var pos = $(this).position();

                $(this).siblings('ul.dropdown-menu').css({
                    top: pos.top + $(this).outerHeight(),
                    left: pos.left
                });

                $(this).siblings('ul.dropdown-menu').show();
            };

            // Hide
            this.hide = function() {
                $(this).siblings('ul.dropdown-menu').hide();
            };

            // Request
            this.request = function() {
                clearTimeout(this.timer);

                this.timer = setTimeout(function(object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            };

            // Response
            this.response = function(json) {
                html = '';

                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }

                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }

                    // Get all the ones with a categories
                    var category = new Array();

                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }

                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }

                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }

                if (html) {
                    this.show();
                } else {
                    this.hide();
                }

                $(this).siblings('ul.dropdown-menu').html(html);
            };

            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

        });
    };

    $('.js-back-link').on('click', function() {
        history.back();
        return false;
    });
})(window.jQuery);