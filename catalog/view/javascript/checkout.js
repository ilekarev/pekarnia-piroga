$(document).ready(function() {

    $('#collapse-checkout-option').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-option" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_option }} <i class="fa fa-caret-down"></i></a>');
    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_payment_address }} <i class="fa fa-caret-down"></i></a>');

    var $modal = $('#js-modal');
    var $sendFormButton = $('#send-form');
    
    var apiGuest = function() {
        var name  = $('#firstname').val(),
        telephone = $('#telephone').val(),
        address   = 'Улица: ' + $('#address_1').val() + ', дом ' + $('#home').val();
    
        var floor    = $('#floor').val(),
            entrance = $('#entrance').val(),
            flat     = $('#flat').val(),
            intercom = $('#intercom').val(),
            datetime = $('#datetime').val();
    
        if (entrance) {
            address += ', подъезд ' + entrance;
        }
        if (floor) {
            address += ', этаж ' + floor;
        }
        if (flat) {
            address += ', квартира ' + flat;
        }
        if (intercom) {
            address += ', домофон ' + intercom;
        }
        if (datetime) {
            address += ', дата и время ' + datetime;
        }
    
        address += ', способ оплаты: ' + $("#pay-m input:checked").val();
    
        var guest = 'customer_group_id=1&firstname=' + name + '&lastname=o&email=mail%40mail.ru&telephone=' + telephone + '&company=&address_1=' + address + '&address_2=&city=Tomsk&postcode=&country_id=176&zone_id=2789&shipping_address=1';
    
        $.ajax({
            url: 'index.php?route=checkout/guest/save',
            type: 'post',
            data: guest,
            dataType: 'json',
            beforeSend: function() {
                // $('#button-guest').button('loading');
            },
            success: function(json) {
                $('.alert-dismissible, .text-danger').remove();
                $('.form-group').removeClass('has-error');
    
                // $('#send-form').val('Отправление заявки (1/7)...');
                // console.log('Отправление заявки (1/7)...');
    
                $.ajax({
                    url: 'index.php?route=checkout/shipping_method',
                    dataType: 'html',
                    complete: function() {
                        // $('#button-guest').button('reset');
                    },
                    success: function(html) {
                        // Add the shipping address
    
                        $('#collapse-shipping-method .panel-body').html(html);
    
                        $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_shipping_method }} <i class="fa fa-caret-down"></i></a>');
    
                        $('a[href=\'#collapse-shipping-method\']').trigger('click');
    
                        $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('{{ text_checkout_payment_method }}');
                        $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('{{ text_checkout_confirm }}');
    
                        // $('#send-form').val('Отправление заявки (2/7)...');
                        // console.log('Отправление заявки (2/7)...');
    
                        apiShippingMethod()
    
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    var apiShippingMethod = function() {
    
        var shipping = 'shipping_method=free.free&comment=';
    
        $.ajax({
            url: 'index.php?route=checkout/shipping_method/save',
            type: 'post',
            data: shipping,
            dataType: 'json',
            beforeSend: function() {
                // $('#button-shipping-method').button('loading');
            },
            success: function(json) {
                $('.alert-dismissible, .text-danger').remove();
    
                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    // $('#button-shipping-method').button('reset');
    
                    if (json['error']['warning']) {
                        $('#collapse-shipping-method .panel-body').prepend('<div class="alert alert-danger alert-dismissible">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                } else {
                    // $('#send-form').val('Отправление заявки (3/7)...');
                    // console.log('Отправление заявки (3/7)...');
    
                    $.ajax({
                        url: 'index.php?route=checkout/payment_method',
                        dataType: 'html',
                        complete: function() {
                            // $('#button-shipping-method').button('reset');
                        },
                        success: function(html) {
                            $('#collapse-payment-method .panel-body').html(html);
    
                            $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_payment_method }} <i class="fa fa-caret-down"></i></a>');
    
                            $('a[href=\'#collapse-payment-method\']').trigger('click');
    
                            $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('{{ text_checkout_confirm }}');
    
                            // $('#send-form').val('Отправление заявки (4/7)...');
                            // console.log('Отправление заявки (4/7)...');
    
                            apiPaymentMethod()
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    var apiPaymentMethod = function() {

        var payment = 'payment_method=cod&comment=';

        var comment  = $('#comment').val();

        if (comment) {
            payment = payment + comment;
        }
    
        $.ajax({
            url: 'index.php?route=checkout/payment_method/save',
            type: 'post',
            data: payment,
            dataType: 'json',
            beforeSend: function() {
                // $('#button-payment-method').button('loading');
            },
            success: function(json) {
                $('.alert-dismissible, .text-danger').remove();
    
                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    // $('#button-payment-method').button('reset');
                    
                    if (json['error']['warning']) {
                        $('#collapse-payment-method .panel-body').prepend('<div class="alert alert-danger alert-dismissible">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                } else {
    
                    // $('#send-form').val('Отправление заявки (5/7)...');
                    // console.log('Отправление заявки (5/7)...');
    
                    $.ajax({
                        url: 'index.php?route=checkout/confirm',
                        dataType: 'html',
                        complete: function() {
                            // $('#button-payment-method').button('reset');
                        },
                        success: function(html) {
                            $('#collapse-checkout-confirm .panel-body').html(html);
    
                            $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-confirm" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_confirm }} <i class="fa fa-caret-down"></i></a>');
    
                            $('a[href=\'#collapse-checkout-confirm\']').trigger('click');
    
                            // $('#send-form').val('Отправление заявки (6/7)...');
                            // console.log('Отправление заявки (6/7)...');
    
                            allDataSended()
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    
    var allDataSended = function() {
        $.ajax({
            url: 'index.php?route=extension/payment/cod/confirm',
            dataType: 'json',
            beforeSend: function() {
    
            },
            complete: function() {
                /* сбросить поля */
            },
            success: function(json) {
                // $('#send-form').val('Отправление заявки (7/7)...');
                // console.log('Отправление заявки (7/7)...');
                if (json['redirect']) {
                    location = json['redirect'];	
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.error(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    
    var beforeSend = function () {
        $modal.addClass('is-active');
    }
    
    $sendFormButton.on('click', function() {
    
        var firstname = $('#firstname').val();
        var telephone = $('#telephone').val();
        var datetime = $('#datetime').val();
        var address_1 = $('#address_1').val();
        var home = $('#home').val();
    
        beforeSend();
    
        if (!((firstname.length > 2) && (telephone.length > 2) && datetime && (address_1.length > 2) && home)) {
    
            $modal.addClass('is-error');
            $sendFormButton.removeAttr('disabled');
    
            $('#js-alert-message').removeClass('is-hidden');
    
            setTimeout(function() {
                $('#js-alert-message').addClass('is-hidden');
            }, 15000);
    
            return false;
        }
    
        $modal.removeClass('is-error');
        $sendFormButton.attr('disabled', true);

        apiGuest()
    });

    $('.js-close-modal').on('click', function() {
        $modal.removeClass('is-active');
    });

    var cleavePhone = new Cleave('#telephone', {
        prefix: '+7',
        phone: true,
        phoneRegionCode: 'RU'
    });

    var cleaveDate = new Cleave('#datetime', {
        delimiters: ['.', '.', ' ', ':'],
        blocks: [2, 2, 4, 2, 2],
    });
    
    


});